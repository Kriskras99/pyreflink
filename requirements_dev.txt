pip
bumpversion
wheel
watchdog
flake8
coverage
Sphinx
mock

pytest
pytest-cov
pytest-runner
