Welcome to Python reflink's documentation!
======================================

Contents:

.. toctree::
   :maxdepth: 2

   readme
   installation
   usage
   modules
   contributing
   authors
   history

Quick start example
-------------------

To use Python reflink in a project::

    from reflink import reflink
    # Reflink copy 'large_file.img' to 'copy_of_file.img'
    reflink("large_file.img", "copy_of_file.img")

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
